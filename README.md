I am an Engineering Manager for the [Engineering Productivity team](https://about.gitlab.com/handbook/engineering/quality/engineering-productivity/) at GitLab. If you see me working strange hours, I work a [non-linear](https://about.gitlab.com/company/culture/all-remote/non-linear-workday/) workday due to family and life commitments. 

